<div id="welcome">
	<div class="row">
		<div class="welcome-wrapper">
			<div class="wlcLeft">
				<div class="container">
					<h1>CRAFT <span>GARAGE DOORS</span> </h1>
					<p>We are the designer, fabricator, and installer. From simple to extravagant, no project is too big or too small. Since the driveway is the entrance to your home, we know that it needs to be beautiful and secure!</p>

					<p>With over 20 years in the business, we will put our experience to work for you! From driveway gates to custom garage doors, we’ve got you covered! </p>
					<a href="about#content" class="btn">READ MORE</a>
				</div>
			</div>
			<div class="wlcRight">
				<img src="public/images/common/section1IMG.jpg" alt="house">
			</div>
		</div>
	</div>
</div>
